#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'jumpingOnClouds' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY c as parameter.
#


def jumpingOnClouds(clouds):
    jumps = 0
    i = 0

    while i < len(clouds) - 1:
        if i + 2 < len(clouds) and clouds[i + 2] == 0:
            i += 2
            jumps += 1
        elif clouds[i + 1] == 0:
            i += 1
            jumps += 1
        else:
            i += 1

    return jumps


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
7
0 0 1 0 0 1 0
"""
# Sample Output
"""
4
"""
