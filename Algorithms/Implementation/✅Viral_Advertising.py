#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'viralAdvertising' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER n as parameter.
#


def viralAdvertising(n):
    j = 5
    list1 = []
    for i in range(n):
        x = j // 2
        j = x * 3
        list1.append(x)
    return sum(list1)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    result = viralAdvertising(n)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
3
"""
# Sample Output
"""
9
"""
