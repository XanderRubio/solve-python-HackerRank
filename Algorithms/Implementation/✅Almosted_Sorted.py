#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'almostSorted' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#


def almostSorted(arr):
    sorted_arr = sorted(arr)
    dif = [(i, a) for i, (a, b) in enumerate(zip(arr, sorted_arr)) if a != b]
    if (
        len(dif) == 2
        and arr[dif[0][0]] == sorted_arr[dif[1][0]]
        and arr[dif[1][0]] == sorted_arr[dif[0][0]]
    ):
        print("yes")
        print("swap", dif[0][0] + 1, dif[1][0] + 1)
    else:
        sublist_to_reverse = arr[dif[0][0] : dif[-1][0] + 1][::-1]
        if sublist_to_reverse == sorted_arr[dif[0][0] : dif[-1][0] + 1]:
            print("yes")
            print("reverse", dif[0][0] + 1, dif[-1][0] + 1)
        else:
            print("no")


if __name__ == "__main__":
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    almostSorted(arr)

# Sample Input
"""
2
4 2
"""
# Sample Output
"""
yes
swap 1 2
"""
