#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'stones' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER a
#  3. INTEGER b
#

from itertools import combinations_with_replacement


def stones(n, a, b):
    p = []
    a = list(combinations_with_replacement([a, b], n))
    for _ in a:
        p.append(sum(_[1:]))
    return sorted(set(p))


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    T = int(input().strip())

    for T_itr in range(T):
        n = int(input().strip())

        a = int(input().strip())

        b = int(input().strip())

        result = stones(n, a, b)

        fptr.write(" ".join(map(str, result)))
        fptr.write("\n")

    fptr.close()


# Sample Input
"""
2
3
1
2
4
10
100
"""
# Sample Output
"""
2 3 4
30 120 210 300
"""
