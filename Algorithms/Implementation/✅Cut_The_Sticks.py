#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'cutTheSticks' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY arr as parameter.
#


def cutTheSticks(arr):
    output = []
    output.append(len(arr))
    if arr.count(min(arr)) > len(set(arr)):
        return output
    else:
        while len(arr) > 1:
            min_stick = min(arr)
            for i in range(arr.count(min_stick)):
                arr.remove(min_stick)
            if len(arr) == 0:
                break
            output.append(len(arr))
            print(arr)

    return output


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = cutTheSticks(arr)

    fptr.write("\n".join(map(str, result)))
    fptr.write("\n")

    fptr.close()

# Sample Input
"""
8
1 2 3 4 3 3 2 1
"""
# Sample Output
"""
8
6
4
1
"""
