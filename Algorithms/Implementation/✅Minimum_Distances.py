#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'minimumDistances' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#


def minimumDistances(a):
    ind = sorted(range(len(a)), key=lambda k: a[k])
    a = sorted(a)
    mini = -1
    for i in range(len(a) - 1):
        if a[i] == a[i + 1]:
            d = ind[i + 1] - ind[i]
            if d < mini or mini == -1:
                mini = d

    return mini


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = minimumDistances(a)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
6
7 1 3 4 1 7
"""
# Sample Output
"""
3
"""
