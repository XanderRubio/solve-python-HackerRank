#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'organizingContainers' function below.
#
# The function is expected to return a STRING.
# The function accepts 2D_INTEGER_ARRAY container as parameter.
#


def organizingContainers(container):
    hsum = [sum(row) for row in container]
    vsum = [
        sum(container[j][i] for j in range(len(container)))
        for i in range(len(container[0]))
    ]

    if all(i in vsum for i in hsum) and all(i in hsum for i in vsum):
        return "Possible"
    else:
        return "Impossible"


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    q = int(input().strip())

    for q_itr in range(q):
        n = int(input().strip())

        container = []

        for _ in range(n):
            container.append(list(map(int, input().rstrip().split())))

        result = organizingContainers(container)

        fptr.write(result + "\n")

    fptr.close()


# Sample Input
"""
2
2
1 1
1 1
2
0 2
1 1
"""

# Sample Output
"""
Possible
Impossible
"""
