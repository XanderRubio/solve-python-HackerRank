#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'gridSearch' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING_ARRAY G
#  2. STRING_ARRAY P
#


def gridSearch(G, P):
    for i in range(len(G) - (len(P) - 1)):
        begin = None
        counter = 0
        for element in P:
            x = re.finditer("(?=" + element + ")", G[i + counter])
            x = [match.start() for match in x]
            if x:
                if begin == None:
                    begin = set(x)
                else:
                    begin = begin & set(x)
                    if not begin:
                        break
                counter += 1
            else:
                break
        if counter == len(P):
            return "YES"

    return "NO"


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    t = int(input().strip())

    for t_itr in range(t):
        first_multiple_input = input().rstrip().split()

        R = int(first_multiple_input[0])

        C = int(first_multiple_input[1])

        G = []

        for _ in range(R):
            G_item = input()
            G.append(G_item)

        second_multiple_input = input().rstrip().split()

        r = int(second_multiple_input[0])

        c = int(second_multiple_input[1])

        P = []

        for _ in range(r):
            P_item = input()
            P.append(P_item)

        result = gridSearch(G, P)

        fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
2
10 10
7283455864
6731158619
8988242643
3830589324
2229505813
5633845374
6473530293
7053106601
0834282956
4607924137
3 4
9505
3845
3530
15 15
400453592126560
114213133098692
474386082879648{-truncated-}
"""
# Sample Output
"""
YES
NO
"""
