#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'extraLongFactorials' function below.
#
# The function accepts INTEGER n as parameter.
#


def extraLongFactorials(n):
    sum_n = 1
    for i in range(n, 0, -1):
        sum_n *= i

    return sum_n


if __name__ == "__main__":
    n = int(input().strip())

    results = extraLongFactorials(n)
    print(results)

# Sample Input
"""
25
"""
# Sample Output
"""
15511210043330985984000000
"""
