#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'kaprekarNumbers' function below.
#
# The function accepts following parameters:
#  1. INTEGER p
#  2. INTEGER q
#


def kaprekarNumbers(p, q):
    flag = False
    for i in range(p, q + 1):
        d = len(str(i))
        sq_str = str(i * i)
        sq_len = len(sq_str)
        right = int(sq_str[sq_len - d :])
        left = int(sq_str[: sq_len - d] or 0)
        if left + right == i:
            print(i, end=" ")
            flag = True
    if not flag:
        print("INVALID RANGE")


if __name__ == "__main__":
    p = int(input().strip())

    q = int(input().strip())

    kaprekarNumbers(p, q)

# Sample Input
"""
1
100
"""
# Sample Output
"""
1 9 45 55 99
"""
