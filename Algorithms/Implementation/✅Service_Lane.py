#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'serviceLane' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER n
#  2. 2D_INTEGER_ARRAY cases
#


def serviceLane(width, cases):
    res = []
    for case in cases:
        res.append(min(width[case[0] : case[1] + 1]))
    return res


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    t = int(first_multiple_input[1])

    width = list(map(int, input().rstrip().split()))

    cases = []

    for _ in range(t):
        cases.append(list(map(int, input().rstrip().split())))

    result = serviceLane(width, cases)

    fptr.write("\n".join(map(str, result)))
    fptr.write("\n")

    fptr.close()

# Sample Input
"""
8 5
2 3 1 2 3 2 3 3
0 3
4 6
6 7
3 5
0 7
"""
# Sample Output
"""
1
2
3
2
1
"""
