#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'fairRations' function below.
#
# The function is expected to return a STRING.
# The function accepts INTEGER_ARRAY B as parameter.
#


def isOdd(i):
    return i % 2 == 1


def fairRations(B):
    odds = sum([1 for numb in B if isOdd(numb)])
    if isOdd(odds):
        return "NO"

    c = 0
    n = len(B)
    for i in range(n):
        if isOdd(B[i]):
            B[i] += 1
            c += 1

            if i < n:
                B[i + 1] += 1
                c += 1

    return str(c)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    N = int(input().strip())

    B = list(map(int, input().rstrip().split()))

    result = fairRations(B)

    fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
5
2 3 4 5 6
"""
# Sample Output
"""
4
"""
