#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'queensAttack' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER k
#  3. INTEGER r_q
#  4. INTEGER c_q
#  5. 2D_INTEGER_ARRAY obstacles
#

def queensAttack(n, k, r_q, c_q, obstacles):
    # Write your code here

    down = r_q - 1
    up = n - r_q
    left = c_q - 1
    right = n - c_q
    q1 = min(up, right)
    q2 = min(up, left)
    q3 = min(down, left)
    q4 = min(down, right)

    if k == 0:
        res = up + down + left + right + q1 + q2 + q3 + q4
        return res

    #up_down: c = c_q
    #left_right: r = r_q
    #q1_q3: r - c = r_q - c_q
    #q2_q4: r + c = r_q + c_q

    mindown, minup, minleft, minright, minq1, minq2, minq3, minq4 = down, up, left, right, q1, q2, q3, q4

    for o in obstacles:
        r, c = o[0], o[1]

        if c == c_q:
            if r > r_q:
                minup = min(minup, r - r_q - 1)
            else:
                mindown = min(mindown, r_q - r - 1)

        elif r == r_q:
            if c < c_q:
                minleft = min(minleft, c_q - c - 1)
            else:
                minright = min(minright, c - c_q - 1)

        elif r - c == r_q - c_q:
            if r > r_q:
                minq1 = min(minq1, r - r_q - 1)
            else:
                minq3 = min(minq3, r_q - r - 1)

        elif r + c == r_q + c_q:
            if r > r_q:
                minq2 = min(minq2, r - r_q - 1)
            else:
                minq4 = min(minq4, r_q - r - 1)

        else:
            continue
    print(mindown, minup, minleft, minright, minq1, minq2, minq3, minq4)
    res = minup + mindown + minleft + minright + minq1 + minq2 + minq3 + minq4

    return res

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    k = int(first_multiple_input[1])

    second_multiple_input = input().rstrip().split()

    r_q = int(second_multiple_input[0])

    c_q = int(second_multiple_input[1])

    obstacles = []

    for _ in range(k):
        obstacles.append(list(map(int, input().rstrip().split())))

    result = queensAttack(n, k, r_q, c_q, obstacles)

    fptr.write(str(result) + '\n')

    fptr.close()

# Sample Input
"""
4 0
4 4
"""
# Sample Output
"""
9
"""
