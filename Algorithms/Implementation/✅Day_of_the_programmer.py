#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'dayOfProgrammer' function below.
#
# The function is expected to return a STRING.
# The function accepts INTEGER year as parameter.
#

from datetime import date, timedelta


def dayOfProgrammer(year):
    # Write your code here
    days = 268
    if year != 1918:
        days = 255 - ((year % 100) == 0 and year < 1918)
    return (date(year, 1, 1) + timedelta(days=days)).strftime("%d.%m.%Y")


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    year = int(input().strip())

    result = dayOfProgrammer(year)

    fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
2017
"""
# Sample Output
"""
13.09.2017
"""
