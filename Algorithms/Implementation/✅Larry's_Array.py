#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'larrysArray' function below.
#
# The function is expected to return a STRING.
# The function accepts INTEGER_ARRAY A as parameter.
#


def larrysArray(A):
    # Count the number of inversions in the array
    inversions = 0
    for i in range(len(A)):
        for j in range(i + 1, len(A)):
            if A[i] > A[j]:
                inversions += 1

    # If the number of inversions is odd, the array cannot be sorted
    if inversions % 2 == 1:
        return "NO"

    # If the number of inversions is even, the array can be sorted
    return "YES"


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        A = list(map(int, input().rstrip().split()))

        result = larrysArray(A)

        fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
3
3
3 1 2
4
1 3 4 2
5
1 2 3 5 4
"""
# Sample Output
"""
YES
YES
NO
"""
