#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'gradingStudents' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY grades as parameter.
#


def gradingStudents(grades):
    rounded_grades = []

    for grade in grades:
        next_multiple = math.ceil(grade / 5) * 5

        if grade < 38 or next_multiple - grade >= 3:
            rounded_grade = grade

        else:
            rounded_grade = next_multiple

        rounded_grades.append(rounded_grade)

    return rounded_grades


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    grades_count = int(input().strip())

    grades = []

    for _ in range(grades_count):
        grades_item = int(input().strip())
        grades.append(grades_item)

    result = gradingStudents(grades)

    fptr.write("\n".join(map(str, result)))
    fptr.write("\n")

    fptr.close()

# Sample Input
"""
4
73
67
38
33
"""
# Sample Output
"""
75
67
40
33
"""
