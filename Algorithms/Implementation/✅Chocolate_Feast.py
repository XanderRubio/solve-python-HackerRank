#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'chocolateFeast' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER c
#  3. INTEGER m
#


def chocolateFeast(n, c, m):
    wrapper = n // c
    chocolate = wrapper

    Flag = True
    while Flag:
        if wrapper >= m:
            offer_validation = wrapper // m
            chocolate = chocolate + offer_validation
            wrapper = offer_validation + (wrapper % m)
        else:
            Flag = False

    return chocolate


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    t = int(input().strip())

    for t_itr in range(t):
        first_multiple_input = input().rstrip().split()

        n = int(first_multiple_input[0])

        c = int(first_multiple_input[1])

        m = int(first_multiple_input[2])

        result = chocolateFeast(n, c, m)

        fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
3
10 2 5
12 4 4
6 2 2
"""
# Sample Output
"""
6
3
"""
