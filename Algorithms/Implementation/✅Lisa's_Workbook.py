#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'workbook' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER k
#  3. INTEGER_ARRAY arr
#


def workbook(n, k, arr):
    special_problems = []
    page_count = 1
    for chap in range(1, n + 1):
        n_probs = arr[chap - 1]
        n_pages_in_chap = (n_probs + k - 1) // k
        max_probs = k * n_pages_in_chap
        for i in range(1, n_probs + 1):
            if i == page_count:
                special_problems.append(page_count)
            if i % k == 0:
                page_count += 1
            if (n_probs == i) and (i < max_probs):
                page_count += 1
    return len(special_problems)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    k = int(first_multiple_input[1])

    arr = list(map(int, input().rstrip().split()))

    result = workbook(n, k, arr)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
5 3
4 2 6 1 10
"""
# Sample Output
"""
4
"""
