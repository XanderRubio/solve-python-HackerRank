#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'equalizeArray' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

from collections import Counter as cnt


def equalizeArray(arr):
    # Write your code here
    minOps = len(arr) - max(cnt(arr).values())
    return minOps


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = equalizeArray(arr)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
5
3 3 2 1 3
"""
# Sample Output
"""
2
"""
