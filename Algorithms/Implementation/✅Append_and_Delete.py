#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'appendAndDelete' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. STRING t
#  3. INTEGER k
#


def appendAndDelete(s, t, k):
    common_len = 0
    for i in range(min(len(s), len(t))):
        if s[i] == t[i]:
            common_len += 1
        else:
            break

    deletion_count = len(s) - common_len
    append_count = len(t) - common_len
    total_operations = deletion_count + append_count

    if k >= total_operations and (k - total_operations) % 2 == 0:
        return "Yes"
    elif k >= len(s) + len(t):
        return "Yes"
    else:
        return "No"


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    s = input()

    t = input()

    k = int(input().strip())

    result = appendAndDelete(s, t, k)

    fptr.write(result + "\n")

    fptr.close()

# Sample Inout
"""
hackerhappy
hackerrank
9
"""
# Sample Output
"""
Yes
"""
