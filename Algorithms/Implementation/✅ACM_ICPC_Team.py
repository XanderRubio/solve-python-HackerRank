#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'acmTeam' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts STRING_ARRAY topic as parameter.
#


def acmTeam(topic):
    # Write your code here
    mymax = 0
    maxes = 0
    for i in range(0, len(topic) - 1):
        for j in range(i + 1, len(topic)):
            if i != j:
                mycount = bin(int(topic[i], base=2) | int(topic[j], base=2)).count("1")
                if mycount == mymax:
                    maxes += 1
                elif mycount > mymax:
                    mymax = mycount
                    maxes = 1
    return (mymax, maxes)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    topic = []

    for _ in range(n):
        topic_item = input()
        topic.append(topic_item)

    result = acmTeam(topic)

    fptr.write("\n".join(map(str, result)))
    fptr.write("\n")

    fptr.close()

# Sample Input
"""
4 5
10101
11100
11010
00101
"""
# Sample Output
"""
5
"""
