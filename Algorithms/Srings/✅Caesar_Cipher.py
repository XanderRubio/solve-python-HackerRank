#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'caesarCipher' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. INTEGER k
#


def caesarCipher(s, n):
    txt = "abcdefghijklmnopqrstuvwxyz" * n
    out = []
    for char in s:
        try:
            idx = txt.index(char.lower()) + n
            final = txt[idx].upper() if char.isupper() else txt[idx]
        except Exception:
            final = char
        out.append(final)
    return "".join(out)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    s = input()

    k = int(input().strip())

    result = caesarCipher(s, k)

    fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
11
middle-Outz
2
"""

# Sample Output
"""
okffng-Qwvb

"""
