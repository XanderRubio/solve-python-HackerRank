#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'hackerrankInString' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def hackerrankInString(str1):
    str = "hackerrank"
    i = 0
    for char in str1:
        if i < len(str) and char == str[i]:
            i += 1
    if i == len(str):
      return "YES"

    else:
      return "NO"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    for q_itr in range(q):
        s = input()

        result = hackerrankInString(s)

        fptr.write(result + '\n')

    fptr.close()

# Sample Input
"""
2
hereiamstackerrank
hackerworld
"""
# Sample Output
"""
YES
NO
"""
