#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'superReducedString' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def superReducedString(s):
    if len(s) < 2: return s

    i,n = 0,len(s)

    while i < n - 1:
        if s[i] == s[i+1]:
            s = s[:i] + s[i+2:]
            i,n = 0,len(s)
            continue

        i += 1

    return "Empty String" if n == 0 else s

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = superReducedString(s)

    fptr.write(result + '\n')

    fptr.close()

# Sample Input
"""
aaabccddd
"""
# Sample Output
"""
abd
"""
