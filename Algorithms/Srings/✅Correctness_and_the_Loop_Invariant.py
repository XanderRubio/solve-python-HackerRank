def insertionSort(arr):
    # Traverse through 1 to len(arr)
    for i in range(1, len(arr)):
        key = arr[i]
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key
    return arr


n = int(input())
s = list(map(int, input().split()))
m = insertionSort(s)
print(*m)

# Sample Input
"""
6
4 1 3 5 6 2
"""
# Sample Output
"""
1 2 3 4 5 6
"""
