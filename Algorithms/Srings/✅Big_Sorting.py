#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'bigSorting' function below.
#
# The function is expected to return a STRING_ARRAY.
# The function accepts STRING_ARRAY unsorted as parameter.
#


def bigSorting(unsorted):
    dic = {}
    for num in unsorted:
        key = len(num)
        if key in dic:
            dic[key].append(num)
        else:
            dic[key] = [num]
    sorted_dic = [sorted(dic[i]) for i in sorted(dic.keys())]
    return [i for sublist in sorted_dic for i in sublist]


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    unsorted = []

    for _ in range(n):
        unsorted_item = input()
        unsorted.append(unsorted_item)

    result = bigSorting(unsorted)

    fptr.write("\n".join(result))
    fptr.write("\n")

    fptr.close()

# Sample Input
"""
6
31415926535897932384626433832795
1
3
10
3
5
"""
# Sample Output
"""
1
3
3
5
10
31415926535897932384626433832795
"""
