#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'alternate' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#


def alternate(s):
    count = 0

    if len(s) > 1:
        for i in range(26):
            for j in range(26):
                a = chr(97 + i)
                b = chr(97 + j)

                if a != b:
                    last_ele = ""
                    str_count = 0
                    flag = True
                    for j in s:
                        if j == a or j == b:
                            if not last_ele or j != last_ele:
                                last_ele = j
                                str_count += 1
                            else:
                                flag = False
                                break

                    if flag:
                        count = max(count, str_count)

    return count


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + "\n")

    fptr.close()
# Sample Input
"""
10
beabeefeab
"""
# Sample Output
"""
5
"""
