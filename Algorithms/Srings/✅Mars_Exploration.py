#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'marsExploration' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def marsExploration(s):
    counter = 0
    sos = ["S", "O", "S"]

    for i in range(len(s)):
        ele = sos[i%3]
        if s[i] != ele:
            counter += 1

    return counter

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = marsExploration(s)

    fptr.write(str(result) + '\n')

    fptr.close()

# Sample Input
"""
SOSSPSSQSSOR
"""

# Sample Output
"""
3
"""
