#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'runningTime' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

def runningTime(arr):
    k = 0
    for i in range(1,  len(arr)):
        for j in range(i):
            if arr[i-j] < arr[i-j-1]:
                arr[i-j], arr[i-j-1] = arr[i-j-1], arr[i-j]
                k += 1
    return k

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = runningTime(arr)

    fptr.write(str(result) + '\n')

    fptr.close()

# Sample Input
"""
5
2 1 3 1 2
"""
# Sample Output
"""
4
"""
