#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'insertionSort2' function below.
#
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER_ARRAY arr
#


def insertionSort2(n, arr):
    for i in range(1, n):
        curr_ele = arr[i]
        count = 0

        if curr_ele < arr[i - 1]:
            for j in range(i, 0, -1):
                if curr_ele < arr[j - 1]:
                    arr[j] = arr[j - 1]
                    arr[j - 1] = curr_ele
                else:
                    break

        print(" ".join(map(str, arr)))


if __name__ == "__main__":
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    insertionSort2(n, arr)

# Sample Input
"""
6
1 4 3 5 6 2
"""

# Sample Output
"""
1 4 3 5 6 2
1 3 4 5 6 2
1 3 4 5 6 2
1 3 4 5 6 2
1 2 3 4 5 6
"""
