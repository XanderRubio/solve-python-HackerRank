#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'timeConversion' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

import datetime


def timeConversion(time_string):
    given_time = datetime.datetime.strptime(time_string, "%I:%M:%S%p")
    return given_time.strftime("%H:%M:%S")


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    time_string = input()

    result = timeConversion(time_string)

    fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
07:05:45PM
"""
# Sample Output
"""
19:05:45
"""
