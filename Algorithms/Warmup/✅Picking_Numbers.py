#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'pickingNumbers' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#


def pickingNumbers(a):
    # Write your code here
    max_length = 0
    a.sort()
    prev_num = 0
    for num in a:
        if num == prev_num:
            continue
        length = max(a.count(num + 1), a.count(num - 1)) + a.count(num)
        if length > max_length:
            max_length = length
        prev_num = num
    return max_length


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = pickingNumbers(a)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
6
4 6 5 3 3 1
"""
# Sample Output
"""
3
"""
