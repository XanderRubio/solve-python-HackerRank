def solveMeFirst(a, b):
    return a + b


num1 = int(input())
num2 = int(input())
res = solveMeFirst(num1, num2)
print(res)

# Sample Input
"""
a = 2
b = 3
"""
# Sample Output
"""
5
"""
