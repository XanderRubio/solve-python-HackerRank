#!/bin/python3

import math
import os
import random
import re
import sys


#
# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#
def miniMaxSum(arr):
    total = sum(arr)

    min_sum = total - max(arr)
    max_sum = total - min(arr)

    print(min_sum, max_sum)


if __name__ == "__main__":
    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)

# Sample Input
"""
1 2 3 4 5
"""
# Sample Output
"""
10 14
"""
