#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#


def calculate_ratios(numbers):
    total_count = len(numbers)

    positive_count = 0
    negative_count = 0
    zero_count = 0

    for num in numbers:
        if num > 0:
            positive_count += 1
        elif num < 0:
            negative_count += 1
        else:
            zero_count += 1

    print(positive_count / total_count)
    print(negative_count / total_count)
    print(zero_count / total_count)


if __name__ == "__main__":
    n = int(input())
    numbers = [int(x) for x in input().split()]

    calculate_ratios(numbers)

# Sample Input
"""
STDIN           Function
-----           --------
6               arr[] size n = 6
-4 3 -9 0 4 1   arr = [-4, 3, -9, 0, 4, 1]
"""
# Sample Output
"""
0.500000
0.333333
0.166667
"""
