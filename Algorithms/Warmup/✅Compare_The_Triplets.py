#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'compareTriplets' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b
#

def compareTriplets(a, b):

    score1 = 0
    score2 = 0

    for i in range(3):
        if a[i] > b[i]:
            score1 += 1
        elif a[i] < b[i]:
            score2 += 1

    return [score1, score2]

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = list(map(int, input().rstrip().split()))

    b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()

# Sample Input
"""
5 6 7
3 6 10
"""
# Sample Output
"""
1 1
"""
