class Multiset:
    def __init__(self):
        self.items = []

    def add(self, val):
        self.items.append(val)

    def remove(self, val):
        if val in self.items:
            self.items.remove(val)

    def __contains__(self, val):
        return val in self.items

    def __len__(self):
        return len(self.items)


# Sample Input
"""
12
query 1
1",...,
add 1
query 1
remove 1
query 1
add 2
add 2
size
quer 2
remove 2
query 2
size
"""
# Sample Output
"""
False
True
False
2
True
True
1
"""
