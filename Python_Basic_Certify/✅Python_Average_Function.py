def avg(*args):
    return sum(args) / len(args)


# Sample Input
"""
2 5
"""
# Sample Output
"""
3.50
"""
