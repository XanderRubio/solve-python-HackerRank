a = int(input())
b = int(input())
result_division = a // b
result_modulo = a % b

print("{}\n{}\n{}".format(result_division, result_modulo, divmod(a, b)))

# Sample Input
"""
177
10
"""
# Sample Output
"""
17
7
(17, 7)
"""
