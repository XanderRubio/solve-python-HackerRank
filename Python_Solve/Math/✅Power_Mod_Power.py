a, b, m = int(input()), int(input()), int(input())
if 1 <= a <= 10 and 1 <= b <= 10 and 2 <= m <= 1000:
    print("{}\n{}".format(pow(a, b), pow(a, b, m)))

# Sample Input
"""
3
4
5
"""

# Sample Output
"""
81
1
"""
