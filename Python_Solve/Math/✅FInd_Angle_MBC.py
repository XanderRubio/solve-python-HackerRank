import math

AB, BC = int(input()), int(input())
if all(0 < x <= 100 for x in (AB, BC)):
    angle = round(
        math.degrees(math.atan2(AB, BC))
    )  # math.atan2 returns angle in radians. Use math.degrees to convert
    print(f"{angle}\N{DEGREE SIGN}")

# Sample Input
"""
10
10
"""
# Sample Output
# 45°
