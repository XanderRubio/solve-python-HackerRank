import cmath

z = complex(input().strip())

r = abs(z)
phi = cmath.phase(z)

print(round(r, 3))
print(round(phi, 3))


# Sample Input
"""
1+2j
"""

# Sample Output
"""
2.23606797749979
1.1071487177940904
"""
