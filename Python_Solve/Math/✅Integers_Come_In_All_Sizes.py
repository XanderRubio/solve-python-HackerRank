a, b, c, d = int(input()), int(input()), int(input()), int(input())

if all(1 <= x <= 1000 for x in (a, b, c, d)):
    print((a**b) + (c**d))

# Sample Input
"""
9
29
7
27
"""
# Sample Output
# 4710194409608608369201743232
