def is_float(N):
    try:
        float(N)
        return True
    except ValueError:
        return False


def is_valid_float(N):
    if N.count(".") != 1:
        return False
    if N[0] not in ["+", "-", "."] and not N[0].isdigit():
        return False
    if not any(c.isdigit() for c in N):
        return False
    return is_float(N)


T = int(input())
for _ in range(T):
    N = input()
    print(is_valid_float(N))

# Sample Input
"""
4
4.0O0
-1.00
+4.54
SomeRandomStuff
"""
# Samole Output
"""
False
True
True
False
"""
