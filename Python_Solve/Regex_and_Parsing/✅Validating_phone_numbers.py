import re

N = int(input())

pattern = r'^[789]\d{9}$'

for i in range(N):
    mobile_number = input()
    if re.match(pattern, mobile_number):
        print('YES')
    else:
        print('NO')

# Sample Input
"""
2
9587456281
1252478965
"""

# Sample Output
"""
YES
NO
"""
