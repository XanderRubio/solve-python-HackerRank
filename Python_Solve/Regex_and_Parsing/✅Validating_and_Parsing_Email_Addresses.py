import re

total_string = "".join(input() + "\n" for i in range(int(input())))

for j in total_string.strip().split("\n"):
    pattern = r"^<[A-Za-z]\w+_*[\.\-]*\w*@[A-Za-z]+\.[A-Za-z]{1,3}>$"
    if re.fullmatch(pattern, j.split(" ")[1]):
        print(j)

# Sample Input
"""
2
DEXTER <dexter@hotmail.com>
VIRUS <virus!@variable.:p>
"""
# Sample Output
"""
DEXTER <dexter@hotmail.com>
"""
