import re

text = []
for _ in range(int(input())):
    text.append(input())
text = "\n".join(text)

text = re.sub(
    r"(?<= )([&]{2}|[|]{2})(?= )", lambda x: "and" if x.group() == "&&" else "or", text
)
print(text)

# Sample Input
"""
11
a = 1;
b = input();

if a + b > 0 && a - b < 0:
    start()
elif a*b > 10 || a/b < 1:
    stop()
print set(list(a)) | set(list(b))
#Note do not change &&& or ||| or & or |
#Only change those '&&' which have space on both sides.
#Only change those '|| which have space on both side
"""
# Sample Output
"""
a = 1;
b = input();

if a + b > 0 and a - b < 0:
    start()
elif a*b > 10 or a/b < 1:
    stop()
print set(list(a)) | set(list(b))
#Note do not change &&& or ||| or & or |
#Only change those '&&' which have space on both sides.
#Only change those '|| which have space on both sides.
"""
