import re


def is_valid(n):
    if (
        len(n) != 10
        or len(set(n)) != 10
        or re.search(r"[\W_]", n)
        or len(re.findall(r"\d", n)) < 3
        or len(re.findall(r"[A-Z]", n)) < 2
    ):
        print("Invalid")
    else:
        print("Valid")


for _ in range(int(input())):
    is_valid(input())

# Sample Input
"""
2
B1CD102354
B1CDEF2354
"""
# Sample Output
"""
"""
