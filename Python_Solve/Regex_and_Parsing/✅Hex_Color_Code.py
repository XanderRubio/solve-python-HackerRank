import re

text = []
for _ in range(int(input())):
    text.append(input())
text = "\n".join(text)

m = re.findall(r"(?<=[^\n])#[a-fA-F0-9]{3,6}", text)
if m:
    for x in m:
        print(x)
else:
    print(-1)

# Sample Input
"""
11
#BED
{
    color: #FfFdF8; background-color:#aef;
    font-size: 123px;
    background: -webkit-linear-gradient(top, #f9f9f9, #fff);
}
#Cab
{
    background-color: #ABC;
    border: 2px dashed #fff;
}
"""
# Sample Output
"""
#FfFdF8
#aef
#f9f9f9
#fff
#ABC
#fff
"""
