import re

S = input()
k = input()

m = re.finditer(f"(?=({k}))", S)

if re.search(k, S):
    for x in m:
        print((x.start(1), x.end(1) - 1))
else:
    print((-1, -1))

# Sample Input
"""
aaadaa
aa
"""
# Sample Output
"""
(0, 1)
(1, 2)
(4, 5)
"""
