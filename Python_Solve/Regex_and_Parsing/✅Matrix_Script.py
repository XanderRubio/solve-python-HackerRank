import re

n, m = map(int, input().split())
lines = [input() for _ in range(n)]
script = "".join("".join(line) for line in zip(*lines))
script = re.sub(r"([a-zA-Z0-9])[^a-zA-Z0-9]+([a-zA-Z0-9])", r"\1 \2", script)

print(script)

# Sample Input
"""
7 3
Tsi
h%x
i #
sM
$a
#t%
ir!
"""
# Sample Output
"""
This is Matrix#  %!
"""
