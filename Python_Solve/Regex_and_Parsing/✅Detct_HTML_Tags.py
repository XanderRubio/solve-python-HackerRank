from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print(tag, *[f"-> {i[0]} > {i[1]}" for i in attrs], sep="\n")

    def handle_startendtag(self, tag, attrs):
        print(tag, *[f"-> {i[0]} > {i[1]}" for i in attrs], sep="\n")


parser = MyHTMLParser()
[parser.feed(input()) for _ in range(int(input()))]

# Sample Input
"""
9
<head>
<title>HTML</title>
</head>
<object type="application/x-flash"
  data="your-file.swf"
  width="0" height="0">
  <!-- <param name="movie" value="your-file.swf" /> -->
  <param name="quality" value="high"/>
</object>
"""
# Sample Output
"""
head
title
object
-> type > application/x-flash
-> data > your-file.swf
-> width > 0
-> height > 0
param
-> name > quality
-> value > high
"""
