from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def handle_comment(self, data):
        print(f">>> Multi-line Comment\n{data}") if "\n" in data else print(
            f">>> Single-line Comment\n{data}"
        )

    def handle_data(self, data):
        if data != "\n":
            print(f">>> Data\n{data}")


N = int(input())
parser = MyHTMLParser()
parser.feed("\n".join([input() for _ in range(N)]))

# Sample Input
"""
4
<!--[if IE 9]>IE9-specific content
<![endif]-->
<div> Welcome to HackerRank</div>
<!--[if IE 9]>IE9-specific content<![endif]-->
"""
# Sample Output
"""
#>>> Multi-line Comment
[if IE 9]>IE9-specific content
<![endif]
#>>> Data
 Welcome to HackerRank
#>>> Single-line Comment
[if IE 9]>IE9-specific content<![endif]
"""
