import re

ccnum_re = re.compile(r"^(?!.*(\d)(-?\1){3})[456]\d{3}(?:-?\d{4}){3}$")
for _ in range(int(input())):
    print("Valid" if ccnum_re.match(input()) else "Invalid")

# Sample Input
"""
6
4123456789123456
5123-4567-8912-3456
61234-567-8912-3456
4123356789123456
5133-3367-8912-3456
5123 - 3567 - 8912 - 3456
"""
# Sample Output
"""
Valid
Valid
Invalid
Valid
Invalid
Invalid
"""
