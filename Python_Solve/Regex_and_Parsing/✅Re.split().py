regex_pattern = r"[,.]"

import re

print("\n".join(re.split(regex_pattern, input())))

# Sample Input
"""
100,000,000.000
"""

# Sample Output
"""
100
000
000
000
"""
