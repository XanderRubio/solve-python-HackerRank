import re

S = input()

matches = re.findall(
    r"(?<=[qwrtypsdfghjklzxcvbnmQWRTYPSDFGHJKLZXCVBNM])[aeiouAEIOU]{2,}(?=[qwrtypsdfghjklzxcvbnmQWRTYPSDFGHJKLZXCVBNM])",
    S,
)

if matches:
    for match in matches:
        print(match)
else:
    print(-1)

# Sample Input
"""
rabcdeefgyYhFjkIoomnpOeorteeeeet
"""
# Sample Output
"""
ee
Ioo
Oeo
eeeee
"""
