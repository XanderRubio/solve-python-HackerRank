from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print("Start :", tag)
        [print(f"-> {attr[0]} > {attr[1]}") for attr in attrs]

    def handle_endtag(self, tag):
        print("End   :", tag)

    def handle_startendtag(self, tag, attrs):
        print("Empty :", tag)
        [print(f"-> {attr[0]} > {attr[1]}") for attr in attrs]


N = int(input())
parser = MyHTMLParser()
parser.feed("\n".join([input() for _ in range(N)]))

# Sample Input
"""
2
<html><head><title>HTML Parser - I</title></head>
<body data-modal-target class='1'><h1>HackerRank</h1><br /></body></html>
"""

# Sample Output
"""
Start : html
Start : head
Start : title
End   : title
End   : head
Start : body
-> data-modal-target > None
-> class > 1
Start : h1
End   : h1
Empty : br
End   : body
End   : html
"""
