K = int(input())
rooms = list(map(int, input().split()))

if 1 < K < 1000:
    room_set = set(rooms)
    captain_room = (sum(room_set) * K - sum(rooms)) // (K - 1)

print(captain_room)

# Sample Input
"""
5
1 2 3 6 5 4 4 2 5 3 6 1 6 5 3 2 4 1 2 5 1 4 3 6 8 4 3 1 5 6 2
"""

# Sample Output
# 8
