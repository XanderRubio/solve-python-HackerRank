A = int(input())
A_set = set(map(int, input().split()))
N = int(input())

if 0 < len(A_set) < 1000 and 0 < N < 100:
    for i in range(N):
        operation, length = input().split()
        length = int(length)
        B_set = set(map(int, input().split()))
        if 0 < len(B_set) < 100:
            if operation == "intersection_update":
                A_set.intersection_update(B_set)
            elif operation == "update":
                A_set.update(B_set)
            elif operation == "symmetric_difference_update":
                A_set.symmetric_difference_update(B_set)
            elif operation == "difference_update":
                A_set.difference_update(B_set)
            else:
                print("Error")
    print(sum(A_set))

# Sample Input
"""
 16
 1 2 3 4 5 6 7 8 9 10 11 12 13 14 24 52
 4
 intersection_update 10
 2 3 5 6 8 9 1 4 7 11
 update 2
 55 66
 symmetric_difference_update 5
 22 7 35 62 58
 difference_update 7
 11 22 35 55 58 62 66
 """

# Sample Output
# 38
