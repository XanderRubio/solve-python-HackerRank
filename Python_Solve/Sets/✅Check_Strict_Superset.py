A = set(input().split())
N = int(input())
is_superset = True

if 0 < len(set(A)) < 501 and 0 < N < 21:
    for i in range(N):
        other_set = set(input().split())
        if 0 < len(other_set) < 101:
            if not other_set.issubset(A) or len(other_set) >= len(A):
                is_superset = False

print(is_superset)

# Sample Input
"""
1 2 3 4 5 6 7 8 9 10 11 12 23 45 84 78
2
1 2 3 4 5
100 11 12
"""

# Sample Output
# False
