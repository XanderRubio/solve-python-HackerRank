def average(array):
    distinct_heights = set(array)
    if 0 < len(distinct_heights) <= 100:
        return round(sum(distinct_heights) / len(distinct_heights), 3)

if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().split()))
    result = average(arr)
    print(result)

# Sample Input
"""
10
161 182 161 154 176 170 167 171 170 174
"""
# Expected Output
# 169.375

# Time complexity of the code is O(n log n)
