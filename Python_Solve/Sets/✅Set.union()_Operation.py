n = int(input())
english = set(map(int, input().split()))
b = int(input())
french = set(map(int, input().split()))

total = len(english.union(french))
print(total)

# Sample Input
"""
9
1 2 3 4 5 6 7 8 9
9
10 1 2 3 11 21 55 6 8
"""

# Sample Output
# 13
