students_english_news = int(input())
english_roll = set(map(int, input().split()))
studetns_french_news = int(input())
french_roll = set(map(int, input().split()))

if 0 < len(english_roll.union(french_roll)) < 1000:
    total = len(english_roll.symmetric_difference(french_roll))
    print(total)

# Sample Input
"""
9
1 2 3 4 5 6 7 8 9
9
10 1 2 3 11 21 55 6 8
"""

# Sample Output
# 8 students

