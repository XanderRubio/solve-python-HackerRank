n = int(input())
stamps = set()

for i in range(n):
    country = input()
    stamps.add(country)

print(len(stamps))

# Sample Input
"""
7
UK
China
USA
France
New Zealand
UK
France
"""

# Sample Output
# 5

# Time complexity: O(n. n is the total number of country stamps
