m = int(input())
set_m = set(map(int, input().split()))
n = int(input())
set_n = set(map(int, input().split()))
symmetric_diff = sorted(set_m.symmetric_difference(set_n))

for i in symmetric_diff:
    print(i)

# Sample Input
"""
4
2 4 5 9
4
2 4 11 12
"""

# Example Output
"""
5
9
11
12
"""

# Time complexity of this code is O((m+n)log(m+n))
