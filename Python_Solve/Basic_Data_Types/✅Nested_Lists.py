if __name__ == "__main__":
    number = int(input())
    students = []
    for i in range(number):
        name = input()
        score = float(input())
        students.append([name, score])

    second_lowest_score = sorted(set([score for name, score in students]))[1]
    second_lowest_students = sorted(
        [name for name, score in students if score == second_lowest_score]
    )
    for student in second_lowest_students:
        print(student)

# Sample input
"""
5
Harry
37.21
Berry
37.21
Tina
37.2
Akriti
41
Harsh
39
"""
