import re


def fun(s):
    return re.match(r"^[A-Za-z0-9_-]+@[a-zA-Z0-9]+\.[a-zA-Z]{,3}$", s)


def filter_mail(emails):
    return list(filter(fun, emails))


if __name__ == "__main__":
    n = int(input())
    emails = []
    for _ in range(n):
        emails.append(input())

filtered_emails = filter_mail(emails)
filtered_emails.sort()
print(filtered_emails)

# Sample Input
"""
3
lara@hackerrank.com
brian-23@hackerrank.com
britts_54@hackerrank.com
"""
# Sample Output
"""
['brian-23@hackerrank.com', 'britts_54@hackerrank.com', 'lara@hackerrank.com']
"""
