import math

N = int(input())
letters = input().split()
K = int(input())

num_a = letters.count("a")
num_of_a = N - num_a

total_combinations = math.comb(N, K)
num_a_combinations = math.comb(num_of_a, K)

prob_num_a = num_a_combinations / total_combinations
prob_least_one_a = 1 - prob_num_a

print("{:.4f}".format(prob_least_one_a))

# Sample Input
"""
4
a a c d
2
"""
# Sample Output
# 0.8333
