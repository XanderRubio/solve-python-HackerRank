from itertools import product

K, M = map(int, input().split())
lists = [list(map(int, input().split()))[1:] for _ in range(K)]

max_S = 0

for combination in product(*lists):
    S = sum(x**2 for x in combination) % M
    if S > max_S:
        max_S = S

print(max_S)


# Sample Input
"""
3 1000
2 5 4
3 7 8 9
5 5 7 8 9 10
"""

# Sample Output
# 206
