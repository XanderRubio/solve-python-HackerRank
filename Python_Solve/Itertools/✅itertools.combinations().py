from itertools import combinations

S, k = input().split()
S = sorted(S)
k = int(k)

if 0 < k <= len(S):
    for i in range(1, k + 1):
        for c in combinations(S, i):
            print("".join(c))

# Sample Input
"""
HACK 2
"""
# Sample Output
"""
A
C
H
K
AC
AH
AK
CH
CK
HK
"""
