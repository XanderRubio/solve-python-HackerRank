from itertools import product


A = list(map(int, input().split()))
B = list(map(int, input().split()))

for a in A:
    for b in B:
        print((a, b), end=" ")


# Sample Input
"""
 1 2
 3 4
"""

# Sample Output
#  (1, 3) (1, 4) (2, 3) (2, 4)
