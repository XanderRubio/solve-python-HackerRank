from itertools import combinations_with_replacement

S, k = input().split()
k = int(k)

if 0 < k <= len(S):
    for c in combinations_with_replacement(sorted(S), k):
        print("".join(c))

# Sample Input
"""
HACK 2
"""

# Sample Output
"""
AA
AC
AH
AK
CC
CH
CK
HH
HK
KK
"""
