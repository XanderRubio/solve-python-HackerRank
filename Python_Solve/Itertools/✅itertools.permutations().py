from itertools import permutations

S, k = input().split()
S = sorted(S)
k = int(k)

if 0 < k <= len(S):
    for p in permutations(S, k):
        print("".join(p))

# Sample Input
"""
HACK 2
"""
# Sample Output
"""
AC
AH
AK
CA
CH
CK
HA
HC
HK
KA
KC
KH
"""
