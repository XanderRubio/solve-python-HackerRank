S = input()
result = ""
count = 1

for i in range(1, len(S)):
    if S[i] == S[i - 1]:
        count += 1
    else:
        result += "({}, {}) ".format(count, S[i - 1])
        count = 1

result += "({}, {})".format(count, S[-1])

print(result)


# Sample INput
"""
1222311
"""

# sample Output
# (1, 1) (3, 2) (1, 3) (2, 1)
