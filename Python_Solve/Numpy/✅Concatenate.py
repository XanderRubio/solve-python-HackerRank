import numpy as np

n, m, p = map(int, input().split())

a = np.array([input().split() for _ in range(n)], int)
b = np.array([input().split() for _ in range(m)], int)

concatenated_array = np.concatenate((a, b), axis=0)

print(concatenated_array)

# Sample Input
"""
4 3 2
1 2
1 2
1 2
1 2
3 4
3 4
3 4
"""
# Sample Output
"""
[[1 2]
[1 2]
[1 2]
[1 2]
[3 4]
[3 4]
[3 4]]
"""
