import numpy as np

shape = tuple(map(int, input().split()))

print(np.zeros(shape, np.int8))

print(np.ones(shape, np.int8))

# Sample Input
"""
3 3 3
"""
# Sample Output
"""
[[[0 0 0]
[0 0 0]
[0 0 0]]

[[0 0 0]
[0 0 0]
[0 0 0]]

[[0 0 0]
[0 0 0]
[0 0 0]]]
[[[1 1 1]
[1 1 1]
[1 1 1]]

[[1 1 1]
[1 1 1]
[1 1 1]]

[[1 1 1]
[1 1 1]
[1 1 1]]]
"""
