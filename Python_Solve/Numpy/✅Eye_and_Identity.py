import numpy

numpy.set_printoptions(legacy="1.13")

nxm = str(input())
n = int(nxm.split(" ")[0])
m = int(nxm.split(" ")[1])

print(numpy.eye(n, m, k=0))

# Sample Input
"""
3 3
"""
# Sample Output
"""
[[ 1.  0.  0.]
[ 0.  1.  0.]
[ 0.  0.  1.]]
"""
