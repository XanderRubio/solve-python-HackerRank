import numpy

N = int(input())

A = numpy.array([input().split() for _ in range(N)], int)
B = numpy.array([input().split() for _ in range(N)], int)

print(numpy.dot(A, B))

# Sample Input
"""
2
1 2
3 4
1 2
3 4
"""
# Sample Output
"""
[[ 7 10]
[15 22]]
"""
