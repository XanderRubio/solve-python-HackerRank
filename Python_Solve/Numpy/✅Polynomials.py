import numpy as np

coefficients = np.array(input().split(), dtype=float)
value = int(input())

result_inner = np.polyval(coefficients, value)
result_outer = np.outer(coefficients, coefficients)

print(result_inner)

# Sample Input
"""
1.1 2 3
0
"""
# Sample Output
"""
3.0
"""
