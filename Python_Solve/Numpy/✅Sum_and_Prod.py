import numpy as np

n, _ = map(int, input().split())

array = np.array([input().split() for _ in range(n)], dtype=int)

result = np.prod(np.sum(array, axis=0))

print(result)

# Sample Input
"""
2 2
1 2
3 4
"""
# Sample Output
"""
24
"""
