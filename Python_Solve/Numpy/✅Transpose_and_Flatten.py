import numpy as np

n, _ = map(int, input().split())

array = []
for i in range(n):
    row = list(map(int, input().split()))
    array.append(row)

array = np.array(array)

transposed_array = np.transpose(array)
flattened_array = array.flatten()

print(transposed_array)
print(flattened_array)

# Sample Input
"""
2 2
1 2
3 4
"""
# Sample Output
"""
[[1 3]
[2 4]]
[1 2 3 4]
"""
