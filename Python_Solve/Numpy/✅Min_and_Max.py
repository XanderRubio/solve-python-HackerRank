import numpy as np

row, column = map(int, input().split())

array = np.zeros((row, column), dtype=int)

for i in range(row):
    array[i] = list(map(int, input().split()))

min_axis_one = np.min(array, axis=1)

print(np.max(min_axis_one))

# Sample Input
"""
4 2
2 5
3 7
1 3
4 0
"""
# Sample Output
"""
3
"""
