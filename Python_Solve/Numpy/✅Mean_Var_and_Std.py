import numpy as np

N, M = map(int, input().split())

my_array = np.array([input().split() for _ in range(N)], dtype=int)

mean_array = np.mean(my_array, axis=1)
var_array = np.var(my_array, axis=0)
std_array = np.std(my_array)

print(mean_array)
print(var_array)
print(round(std_array, 11))

# Sample Input
"""
2 2
1 2
3 4
"""
# Sample Output
"""
[ 1.5  3.5]
[ 1.  1.]
1.11803398875
"""
