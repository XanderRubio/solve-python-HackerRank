import numpy as np

N, _ = map(int, input().split())

A = np.array([list(map(int, input().split())) for _ in range(N)])
B = np.array([list(map(int, input().split())) for _ in range(N)])

operations = [A + B, A - B, A * B, A // B, A % B, A**B]

for operation in operations:
    print(operation)

# Sample Input
"""
1 4
1 2 3 4
5 6 7 8
"""
# Sample Output
"""
[[ 6  8 10 12]]
[[-4 -4 -4 -4]]
[[ 5 12 21 32]]
[[0 0 0 0]]
[[1 2 3 4]]
[[    1    64  2187 65536]]
"""
