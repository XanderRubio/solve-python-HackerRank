import numpy as np

n = int(input())
m = np.array([input().split() for _ in range(n)], dtype=float)

determinant = np.linalg.det(m)

print(round(determinant, 2))

# Sample Input
"""
2
1.1 1.1
1.1 1.1
"""
# Sample Output
"""
0.0
"""
