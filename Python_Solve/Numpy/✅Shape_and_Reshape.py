import numpy

lst = list(map(int, input().split()))
array = numpy.array(lst)

print(numpy.reshape(array, (3, 3)))

# Sample Input
"""
1 2 3 4 5 6 7 8 9
"""
# Sample Output
"""
[[1 2 3]
[4 5 6]
[7 8 9]]
"""
