import calendar

MM, DD, YYYY = map(int, input().split())
day_of_the_week = calendar.weekday(YYYY, MM, DD)
name_day = calendar.day_name[day_of_the_week]
print(name_day.upper())

# Sample Input
"""
08 05 2015
"""

# Sample Output
"""
WEDNESDAY
"""
