def wrapper(f):
    def fun(l):
        f([f"+91 {i[-10:-5]} {i[-5::]}" for i in l])

    return fun


@wrapper
def sort_phone(l):
    print(*sorted(l), sep="\n")


if __name__ == "__main__":
    l = [input() for _ in range(int(input()))]
    sort_phone(l)

# Sample Input
"""
3
07895462130
919875641230
9195969878
"""
# Sample Output
"""
+91 78954 62130
+91 91959 69878
+91 98756 41230
"""
