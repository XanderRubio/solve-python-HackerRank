x, k = list(map(int, input().split(" ")))
p = input()
res = eval(p)
print(True if res == k else False)

# Sample Input
"""
1 4
x**3 + x**2 + x + 1
"""
# Sample Output
"""
True
"""
