N = int(input())
lst = list(map(int, input().split()))
print(all(i > 0 for i in lst) and any(str(i) == str(i)[::-1] for i in lst))

# Sample Input
"""
5
12 9 61 5 14
"""
# Sample Output
"""
True
"""
