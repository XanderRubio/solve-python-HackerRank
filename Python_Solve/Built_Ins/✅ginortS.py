S = input()
sorted_S = sorted(S, key=lambda x: (x.isdigit(), x.isdigit() and int(x) % 2 == 0, x.isupper(), x))
print("".join(sorted_S))

# Sample Input
"""
Sorting1234
"""
# Sample Output
"""
ginortS1324
"""
