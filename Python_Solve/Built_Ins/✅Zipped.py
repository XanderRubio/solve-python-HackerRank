N, X = map(int, input().split())
scores = []

for i in range(X):
    subject_scores = list(map(float, input().split()))
    scores.append(subject_scores)

for i in range(N):
    total_score = sum([scores[j][i] for j in range(X)])
    average_score = total_score / X
    print("{:.1f}".format(average_score))

# Sample Input
"""
5 3
89 90 78 93 80
90 91 85 88 86
91 92 83 89 90.5
"""
# Sample Output
"""
90.0
91.0
82.0
90.0
85.5
"""
