# Had to run this code in Pypy3 compiler to pass tests
import re

for _ in range(int(input())):
    s = input()
    try:
        re.compile(s)
        print(True)
    except re.error:
        print(False)

# Sample Input
"""
2
.*\+
.*+
"""
# Sample Output
"""
True
False
"""
