def minion_game(string):
    if 0 < len(string) <= 10**6:
        vowels = "AEIOU"
        Stuart_score = 0
        Kevin_score = 0
        length_of_word = len(string)

        for i in range(length_of_word):
            if string[i] in vowels:
                Kevin_score += length_of_word - i
            else:
                Stuart_score += length_of_word - i

        if Kevin_score > Stuart_score:
            print("Kevin", Kevin_score)
        elif Stuart_score > Kevin_score:
            print("Stuart", Stuart_score)
        else:
            print("Draw")


if __name__ == "__main__":
    s = input()
    minion_game(s)

# Sample Input
"""
BANANA
"""

# Sample Output = Stuart 12
# Time Complexity is O(n^2)
