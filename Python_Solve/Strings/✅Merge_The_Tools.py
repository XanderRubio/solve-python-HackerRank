def merge_the_tools(string, k):
    word = len(string)
    num_substrings = word // k
    substings = [string[i * k : (i + 1) * k] for i in range(num_substrings)]

    for substring in substings:
        unique_chars = []
        for char in substring:
            if char not in unique_chars:
                unique_chars.append(char)
        print("".join(unique_chars))


if __name__ == "__main__":
    string, k = input(), int(input())
    merge_the_tools(string, k)

# Sample input
"""
AABCAAADA
3
"""
# Sample output
"""
AB
CA
AD
"""
# Time complexity of O^n
