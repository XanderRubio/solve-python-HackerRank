def door_mat(N, M):
    if N % 2 == 1 and M == (N * 3) and 5 < N < 101 and 15 < M < 303:
        design = ""
        for i in range(N):
            if i < N // 2:
                pattern = ".|." * (2 * i + 1)
            elif i == N // 2:
                pattern = "WELCOME"
            else:
                pattern = ".|." * (2 * (N - i - 1) + 1)
            design += pattern.center(M, "-") + "\n"
        return design
    else:
        return "Invalid"


if __name__ == "__main__":
    input_str = input()
    N, M = map(int, input_str.split())
    result = door_mat(N, M)
    if result == "Invalid":
        print(result)
    else:
        print(result.rstrip())


# Sample Input
"""
9 27
"""


