import math
import os
import random
import re
import sys


# Complete the solve function below.
def solve(s):
    if 0 < len(s) < 1000:
        isFirstCharacter = True
        isPreviousCharacterSpace = False

        result = str()
        for character in s:
            if isFirstCharacter:
                character = character.upper()
                result = result + character
                isFirstCharacter = False
            else:
                if character.isspace():
                    result = result + character
                    isPreviousCharacterSpace = True
                else:
                    if isPreviousCharacterSpace:
                        result = result + character.upper()
                        isPreviousCharacterSpace = False
                    else:
                        result = result + character
        return result


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    s = input()

    result = solve(s)

    fptr.write(result + "\n")

    fptr.close()
