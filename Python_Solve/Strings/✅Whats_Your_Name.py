def print_full_name(first, last):
    F = first
    L = last
    if len(F) <= 10 and len(L) <= 10:
        print(f"Hello {F} {L}! You just delved into python.")


if __name__ == "__main__":
    first_name = input()
    last_name = input()
    print_full_name(first_name, last_name)


# Sample Input
"""
Ross
Taylor
"""
