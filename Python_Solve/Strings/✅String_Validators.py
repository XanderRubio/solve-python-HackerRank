if __name__ == "__main__":
    string = input()
    if 0 < len(string) < 1000:
        alnum = any(char.isalnum() for char in string)
        alpha = any(char.isalpha() for char in string)
        digit = any(char.isdigit() for char in string)
        lower = any(char.islower() for char in string)
        upper = any(char.isupper() for char in string)
        print(alnum)
        print(alpha)
        print(digit)
        print(lower)
        print(upper)

# Sample input
"""
qA2
"""
