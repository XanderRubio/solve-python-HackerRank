def print_rangoli(size):
    asc = 96 + size
    length = size * 4 - 3
    List = []
    for i in range(1, size + 1):  # O(n)
        if n == 1:
            letter = chr(asc)
        else:
            letter = chr(asc) + "-"
        for j in range(1, i * 2 - 1):  # O(n)
            if j < i:
                asc -= 1
                letter = letter + chr(asc) + "-"
            elif i == size and j == i * 2 - 2:
                asc += 1
                letter = letter + chr(asc)
            elif j >= i:
                asc += 1
                letter = letter + chr(asc) + "-"
        List.append(letter.center(length, "-"))
    print("\n".join(List))
    print("\n".join([List[i] for i in range(size - 2, -1, -1)]))


if __name__ == "__main__":
    n = int(input())
    print_rangoli(n)

# Sample Input
"""
5
"""

# O(n^2), which means that the execution time of the code will increase quadratically with the input size
