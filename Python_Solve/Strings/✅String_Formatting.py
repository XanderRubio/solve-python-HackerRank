def print_formatted(number):
    if 1 <= n <= 99:
        width = len("{0:b}".format(number))
        for i in range(1, n + 1):
            Decimal = str(i).rjust(width)
            Octal = "{0:o}".format(i).rjust(width)
            Hexadecimal = "{0:X}".format(i).rjust(width)
            Binary = "{0:b}".format(i).rjust(width)
            print(Decimal, Octal, Hexadecimal, Binary)
    else:
        return "invalid"


if __name__ == "__main__":
    n = int(input())
    print_formatted(n)

# Sample Input
"""
17
"""
