from collections import deque

N = int(input())
d = deque()

for i in range(N):
    operation, *value = input().split()
    getattr(d, operation)(*value)

print(*d)

# Sample Input
"""
6
append 1
append 2
append 3
appendleft 4
pop
popleft
"""
# Sample Output
"""
1 2
"""
