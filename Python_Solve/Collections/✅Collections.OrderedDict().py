from collections import OrderedDict

n = int(input())
items = OrderedDict()

for i in range(n):
    item, price = input().rsplit(maxsplit=1)
    items[item] = items.get(item, 0) + int(price)

for item, price in items.items():
    print(item, price)

# Sample Input
"""
9
BANANA FRIES 12
POTATO CHIPS 30
APPLE JUICE 10
CANDY 5
APPLE JUICE 10
CANDY 5
CANDY 5
CANDY 5
POTATO CHIPS 30
"""
# Sample Output
"""
BANANA FRIES 12
POTATO CHIPS 60
APPLE JUICE 20
CANDY 20
"""

# Time complexity is O(n log n)
