from collections import Counter

s = input()
most_common = sorted(Counter(sorted(s)).items(), key=lambda x: x[1], reverse=True)
for i in range(3):
    print(*most_common[i])

# Sample Input
"""
aabbbccde
"""

# Sample Output
"""
b 3
a 2
c 2
"""
