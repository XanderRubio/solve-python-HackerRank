from collections import Counter

X = int(input())
shoe_size = list(map(int, input().split()))
N = int(input())

earnings = 0
counter = Counter(shoe_size)

for n in range(N):
    size, price = map(int, input().split())
    if counter[size] > 0:
        earnings += price
        counter[size] -= 1

print(earnings)

# Sample Input
"""
10
2 3 4 5 6 8 7 6 5 18
6
6 55
6 45
6 55
4 40
18 60
10 50
"""
# Sample Output
# 200
