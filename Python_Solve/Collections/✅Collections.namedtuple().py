from collections import namedtuple

n = int(input())
columns = input().split()
Student = namedtuple("Student", columns)

total_marks = sum([int(Student(*input().split()).MARKS) for _ in range(n)])

print("{:.2F}".format(total_marks / n))

# Sample Input
"""
5
ID         MARKS      NAME       CLASS
1          97         Raymond    7
2          50         Steven     4
3          91         Adrian     9
4          72         Stewart    5
5          80         Peter      6
"""

# Sample Output
# 78.00
