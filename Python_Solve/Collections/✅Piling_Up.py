from collections import deque


def check_pile_possible(num_cubes, cube_sidelength):
    pile = deque(cube_sidelength)
    prev_cube = float("inf")

    while pile:
        leftmost_cube = pile[0]
        rightmost_cube = pile[-1]

        if leftmost_cube >= rightmost_cube and leftmost_cube <= prev_cube:
            prev_cube = pile.popleft()
        elif rightmost_cube >= leftmost_cube and rightmost_cube <= prev_cube:
            prev_cube = pile.pop()
        else:
            return "No"

    return "Yes"


if __name__ == "__main__":
    num_tests = int(input().strip())

    for i in range(num_tests):
        num_cubes = int(input().strip())
        cube_sidelengths = list(map(int, input().strip().split()))
        result = check_pile_possible(num_cubes, cube_sidelengths)
        print(result)

# Sample Input
"""
2            T = 2
6            blocks[] size n = 6
4 3 2 1 3 4  blocks = [4, 3, 2, 1, 3, 4]
3            blocks[] size n = 3
1 3 2        blocks = [1, 3, 2]
"""
# Sample Output
"""
Yes
No
"""
# Time Complexity is O(n)
