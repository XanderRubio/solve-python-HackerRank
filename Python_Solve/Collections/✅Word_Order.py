n = int(input())
words = []
word_counts = {}

for i in range(n):
    word = input().strip()
    words.append(word)
    word_counts[word] = word_counts.get(word, 0) + 1

print(len(word_counts))

for word in words:
    if word_counts[word] > 0:
        print(word_counts[word], end=" ")
        word_counts[word] = 0

# Sample Input
"""
4
bcdef
abcdefg
bcde
bcdef
"""
# Sample Output
"""
3
2 1 1
"""
