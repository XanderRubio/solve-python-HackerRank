from collections import defaultdict

n, m = map(int, input().split())

group_a = defaultdict(list)

# Reading the words that belong to group A
for i in range(n):
    word = input().strip()
    group_a[word].append(i + 1)

# Reading the words that belong to group B
for i in range(m):
    word = input().strip()
    occurences = group_a[word]
    if occurences:
        print(" ".join(map(str, occurences)))
    else:
        print(-1)

# Sample Input
"""
5 2
a
a
b
a
b
a
b
"""
# Sample Output
"""
1 2 4
3 5
"""
