WITH a AS (SELECT DISTINCT submission_date, hacker_id, DENSE_RANK() OVER (PARTITION BY hacker_id ORDER BY submission_date) AS rn FROM Submissions)
,b AS (SELECT submission_date, COUNT(hacker_id)as dh FROM a WHERE DAY(submission_date) = rn GROUP BY submission_date)
,d AS (SELECT submission_date, hacker_id, COUNT(hacker_id) AS ch,row_number() over (partition by submission_date order by COUNT(hacker_id) DESC, hacker_id) AS rn FROM Submissions
      GROUP BY submission_date, hacker_id)
SELECT d.submission_date, b.dh, d.hacker_id, H.name FROM Hackers AS H
INNER JOIN d ON d.hacker_id = H.hacker_id
INNER JOIN b ON b.submission_date = d.submission_date
WHERE d.rn = 1
ORDER BY d.submission_date
