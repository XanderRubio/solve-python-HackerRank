SELECT
    [Doctor], [Professor], [Singer], [Actor]
FROM
    (
        SELECT
            Occupation,
            Name,
            ROW_NUMBER() OVER (PARTITION BY Occupation ORDER BY Name) AS NameOrder
        FROM
            OCCUPATIONS
    ) AS SourceTable
PIVOT
    (
        MAX(Name)
        FOR Occupation IN ([Doctor], [Professor], [Singer], [Actor])
    ) AS PivotTable
ORDER BY NameOrder;
