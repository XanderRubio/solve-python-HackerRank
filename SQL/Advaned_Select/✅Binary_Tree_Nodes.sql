SELECT
  n,
  IF(p IS NULL, 'Root',
    IF(n IN (SELECT p FROM bst), 'Inner', 'Leaf')) AS type
FROM bst
ORDER BY n;
