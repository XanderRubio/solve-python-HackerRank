SELECT Challenge_ID,
       SUM(Total_Views) AS Total_Views,
       SUM(Total_Unique_Views) AS Total_Unique_Views
INTO #ViewStats
FROM View_Stats
GROUP BY Challenge_ID

SELECT Challenge_ID,
       SUM(Total_Submissions) AS Total_Submissions,
       SUM(Total_Accepted_Submissions) AS Total_Accepted_Submissions
INTO #SubmissionStats
FROM Submission_Stats
GROUP BY Challenge_ID

SELECT CO.Contest_ID,CO.Hacker_ID,CO.Name,
       COALESCE(SUM(Total_Submissions),0),
       COALESCE(SUM(Total_Accepted_Submissions),0),
       COALESCE(SUM(Total_Views),0),
       COALESCE(SUM(Total_Unique_Views),0)
FROM Contests CO
JOIN Colleges CL
    ON CO.Contest_ID = CL.Contest_ID
JOIN Challenges CH
    ON CH.College_ID = CL.College_ID
LEFT JOIN #ViewStats VS
    ON VS.Challenge_ID = CH.Challenge_ID
LEFT JOIN #SubmissionStats SS
    ON SS.Challenge_ID = CH.Challenge_ID
GROUP BY CO.Contest_ID,CO.Hacker_ID,CO.Name
HAVING (COALESCE(SUM(Total_Submissions),0)+
       COALESCE(SUM(Total_Accepted_Submissions),0)+
       COALESCE(SUM(Total_Views),0)+
       COALESCE(SUM(Total_Unique_Views),0))>0
ORDER BY CO.Contest_ID
