SELECT
    S.Name
FROM
    Friends AS F
INNER JOIN
    Students AS S
        ON S.ID = F.ID
INNER JOIN
    Packages AS P
        ON P.ID = F.ID
INNER JOIN
    Packages AS P2
        ON P2.ID = F.Friend_ID
WHERE
    P2.Salary > P.Salary
ORDER BY
    P2.Salary;
