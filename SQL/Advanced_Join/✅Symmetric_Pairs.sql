SELECT x, y
FROM (
  SELECT f1.x, f1.y
  FROM functions f1
  JOIN functions f2 ON f1.x = f2.y AND f1.y = f2.x AND f1.x != f1.y AND f2.x != f2.y
  WHERE f1.x <= f1.y
  GROUP BY f1.x, f1.y
) AS inner_join
UNION ALL
SELECT x, y
FROM functions
WHERE x = y
GROUP BY x, y
HAVING COUNT(*) > 1
ORDER BY x ASC

