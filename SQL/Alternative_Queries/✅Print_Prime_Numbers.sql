DECLARE @I INT;
DECLARE @J INT;
DECLARE @COUNT INT;
DECLARE @FINAL VARCHAR(1000);
SET @I = 2;
SET @FINAL = '';

WHILE @I < 1000
BEGIN
    SET @J = 2;
    SET @COUNT = 0;
    WHILE @J <= SQRT(@I)
    BEGIN
        IF @I%@J = 0
        BEGIN
            SET @COUNT = 1;
            BREAK;
        END
        SET @J = @J + 1
    END
    IF @COUNT = 0
    BEGIN
        SET @FINAL = CONCAT(@FINAL, CAST(@I AS VARCHAR), '&');
    END
    SET @I = @I + 1;
END
SELECT SUBSTRING(@FINAL, 1, LEN(@FINAL)-1);
