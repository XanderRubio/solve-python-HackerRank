SELECT s.hacker_id, h.name
FROM Submissions AS s
INNER JOIN Challenges AS c ON s.challenge_id = c.challenge_id
JOIN Difficulty AS d ON d.difficulty_level = c.difficulty_level
JOIN Hackers AS h ON s.hacker_id = h.hacker_id
WHERE s.score = d.score
GROUP BY s.hacker_id, h.name
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC, s.hacker_id;
