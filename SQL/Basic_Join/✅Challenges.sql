with t as
(
    select h.hacker_id, name, count(challenge_id) as cnt
    from hackers as h join challenges as c on h.hacker_id = c.hacker_id
    group by hacker_id, name
)

select * from t
having cnt in (select cnt from t
               group by cnt
               having count(hacker_id) = 1 or cnt = (select max(cnt) from t) )

order by cnt desc, hacker_id;
