SELECT s.hacker_id, h.name, SUM(s.max_score) AS total_score
FROM (
    SELECT hacker_id, challenge_id, MAX(score) AS max_score
    FROM submissions
    WHERE score != 0
    GROUP BY hacker_id, challenge_id
) s
JOIN hackers h ON s.hacker_id = h.hacker_id
GROUP BY s.hacker_id, h.name
HAVING SUM(s.max_score) > 0
ORDER BY total_score DESC, s.hacker_id;
