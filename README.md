# Solve Python HackerRank

 Documenting my solutions for completing the Python preperation kit and 1 week preperation kit for coding challenges at https://www.hackerrank.com/. My daily [documentation log can be viewed here](https://gitlab.com/XanderRubio/HackChallenger/-/blob/main/Daily_Documentation/Photos_Log/Log.md) for verification of coding everyday.
