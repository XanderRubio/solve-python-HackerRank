def truckTour(petrolpumps):
    start = 0
    total_petrol = 0

    for i in range(len(petrolpumps)):
        petrol, distance = petrolpumps[i]
        total_petrol += petrol - distance

        if total_petrol < 0:
            start = i + 1
            total_petrol = 0

    if total_petrol >= 0:
        return start
    else:
        return -1


# Sample Input
"""
3
1 5
10 3
3 4
"""
# Sample Output
"""
1
"""
