#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'minimumBribes' function below.
#
# The function accepts INTEGER_ARRAY q as parameter.
#


def minimumBribes(array):
    total_bribes = 0
    too_chaotic = False

    for index in range(len(array)):
        if array[index] - (index + 1) > 2:
            print("Too chaotic")
            return

        for j in range(max(0, array[index] - 2), index):
            if array[j] > array[index]:
                total_bribes += 1

    print(total_bribes)


if __name__ == "__main__":
    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        q = list(map(int, input().rstrip().split()))

        minimumBribes(q)

# Sample Input
"""
2
5
2 1 5 3 4
5
2 5 1 3 4
"""
# Sample Output
"""
3
Too chaotic
"""
