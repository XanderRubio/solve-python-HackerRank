#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'noPrefix' function below.
#
# The function accepts STRING_ARRAY words as parameter.
#


def insert(trie, word):
    for i, char in enumerate(word):
        if char in trie:
            if trie[char].get("is_end") or i == len(word) - 1:
                return True
            else:
                trie = trie[char]
        else:
            trie[char] = {}
            trie = trie[char]

    trie["is_end"] = True  # Marquer la fin du mot
    return False


def noPrefix(words):
    trie = {}
    for word in words:
        if insert(trie, word):
            print("BAD SET")
            print(word)
            return
    print("GOOD SET")


if __name__ == "__main__":
    n = int(input().strip())

    words = []

    for _ in range(n):
        words_item = input()
        words.append(words_item)

    noPrefix(words)

# Sample Input
"""
4
aab
aac
aacghgh
aabghgh
"""

# Sample Output
"""
BAD SET
aacghgh
"""
