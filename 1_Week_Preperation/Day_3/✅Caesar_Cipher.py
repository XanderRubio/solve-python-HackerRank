#!/bin/python3

import math
import os
import random
import re
import sys
import string

#
# Complete the 'caesarCipher' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. INTEGER k
#


def caesarCipher(s, k):
    result = []
    for i in range(len(s)):
        if s[i].islower():
            idx = (string.ascii_lowercase.index(s[i]) + k) % 26
            result.append(string.ascii_lowercase[idx])
        elif s[i].isupper():
            idx = (string.ascii_uppercase.index(s[i]) + k) % 26
            result.append(string.ascii_uppercase[idx])
        else:
            result.append(s[i])
    return "".join(result)


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    s = input()

    k = int(input().strip())

    result = caesarCipher(s, k)

    fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
11
middle-Outz
2
"""
# Sample Output
"""
okffng-Qwvb
"""
