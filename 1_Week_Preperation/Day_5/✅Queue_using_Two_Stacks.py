from collections import deque


def process_queries():
    query_count = int(input())
    queue = deque()
    for _ in range(query_count):
        query = input().split()
        if query[0] == "1":
            queue.append(int(query[1]))
        elif query[0] == "2":
            if queue:
                queue.popleft()
            else:
                print("Queue empty.")
        else:
            if queue:
                print(queue[0])
            else:
                print("Queue empty.")


process_queries()

# Sample Input
"""
STDIN   Function
-----   --------
10      q = 10 (number of queries)
1 42    1st query, enqueue 42
2       dequeue front element
1 14    enqueue 42
3       print the front element
1 28    enqueue 28
3       print the front element
1 60    enqueue 60
1 78    enqueue 78
2       dequeue front element
2       dequeue front element
"""
# Sample Output
"""
14
14
"""
