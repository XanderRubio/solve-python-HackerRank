#!/bin/python3

import math
import os
import random
import re
import sys


class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None


class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node

        self.tail = node


def print_singly_linked_list(node, sep, fptr):
    while node:
        fptr.write(str(node.data))

        node = node.next

        if node:
            fptr.write(sep)


# Complete the mergeLists function below.


#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next
#
#
def mergeLists(head1, head2):
    if not head1 or not head2:
        return head1 or head2

    merged_head = current = None

    while head1 and head2:
        smaller_node = head1 if head1.data <= head2.data else head2
        head1, head2 = (
            (head1.next, head2) if head1.data <= head2.data else (head1, head2.next)
        )

        if not merged_head:
            merged_head = current = smaller_node
        else:
            current.next = smaller_node
            current = smaller_node

    current.next = head1 or head2

    return merged_head


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    tests = int(input())

    for tests_itr in range(tests):
        llist1_count = int(input())

        llist1 = SinglyLinkedList()

        for _ in range(llist1_count):
            llist1_item = int(input())
            llist1.insert_node(llist1_item)

        llist2_count = int(input())

        llist2 = SinglyLinkedList()

        for _ in range(llist2_count):
            llist2_item = int(input())
            llist2.insert_node(llist2_item)

        llist3 = mergeLists(llist1.head, llist2.head)

        print_singly_linked_list(llist3, " ", fptr)
        fptr.write("\n")

    fptr.close()

# Sample Input
"""
1
3
1
2
3
2
3
4
"""
# Sample Output
"""
1 2 3 3 4
"""
