#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'isBalanced' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#


def isBalanced(s):
    stack = []
    d = {
        "(": ")",
        "[": "]",
        "{": "}",
    }
    for c in s:
        if c in d.keys():
            stack.append(c)
        elif c in d.values():
            if not stack or c != d[stack.pop()]:
                return "NO"
    return "NO" if stack else "YES"


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    t = int(input().strip())

    for t_itr in range(t):
        s = input()

        result = isBalanced(s)

        fptr.write(result + "\n")

    fptr.close()

# Sample Input
"""
STDIN Function ----- -------- 3 n = 3 {[()]} first s = '{[()]}' {[(])} second s = '{[(])}' {{[[(())]]}} third s ='{{[[(())]]}}'
"""
# Sample Output
"""
YES
NO
YES
"""
