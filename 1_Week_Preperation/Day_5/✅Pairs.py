def count_pairs(arr, k):
    count = 0
    num_set = set(arr)
    for num in arr:
        if num + k in num_set:
            count += 1
    return count


# Sample Input
"""
5 2
"""
# Sample Output
"""
1 5 3 4 2
"""
