#!/bin/python3

import math
import os
import random
import re
import sys
from queue import PriorityQueue

#
# Complete the 'cookies' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER k
#  2. INTEGER_ARRAY A
#


def cookies(k, A):
    pq = PriorityQueue()
    for a in A:
        pq.put(a)

    counter = 0
    while pq.queue[0] < k:
        if pq.qsize() == 1:
            return -1

        x = pq.get()
        y = pq.get()
        pq.put(x + 2 * y)
        counter += 1

    return counter


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    k = int(first_multiple_input[1])

    A = list(map(int, input().rstrip().split()))

    result = cookies(k, A)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Inout
"""
STDIN               Function
-----               --------
6 7                 A[] size n = 6, k = 7
1 2 3 9 10 12       A = [1, 2, 3, 9, 10, 12]
"""
# Sample Output
"""
2
"""
