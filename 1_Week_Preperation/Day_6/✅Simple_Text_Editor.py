q = int(input())
result = ""
deleted = []
stack = []
for i in range(q):
    t = input().split()
    if int(t[0]) == 1:
        string = str(t[1])
        deleted.append(str(string))
        result = result + string
        stack.extend([1])
    if int(t[0]) == 2:
        d = int(t[1])
        deleted.append(str(result[-d:]))
        result = result[:-d]
        stack.extend([2])
    if int(t[0]) == 3:
        i = int(t[1])
        print(result[i - 1])
    if int(t[0]) == 4:
        pop = int(stack.pop())
        if pop == 1:
            length = len(str(deleted.pop()))
            result = result[:-length]
        elif pop == 2:
            string = deleted.pop()
            result = result + string

    # Sample Input
    """
    STDIN   Function
-----   --------
8       Q = 8
1 abc   ops[0] = '1 abc'
3 3     ops[1] = '3 3'
2 3     ...
1 xy
3 2
4
4
3 1
"""
# Sample Output
"""
c
y
a
"""
