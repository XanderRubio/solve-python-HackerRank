import collections


def bfs(n, m, edges, start):
    # Create an adjacency list to represent the graph
    graph = collections.defaultdict(list)
    for edge in edges:
        u, v = edge
        graph[u].append(v)
        graph[v].append(u)

    # Initialize distances with -1 (unreachable)
    distances = [-1] * (n + 1)
    distances[start] = 0

    # Perform breadth-first search
    queue = collections.deque([start])
    while queue:
        node = queue.popleft()
        for neighbor in graph[node]:
            if distances[neighbor] == -1:
                distances[neighbor] = distances[node] + 6
                queue.append(neighbor)

    # Return distances to nodes in increasing node number order
    return distances[1:start] + distances[start + 1 :]


# Sample Input
"""
2
4 2
1 2
1 3
a
3 1
2 3
2
"""
# Sample Output
"""
6 6 -1
-1 6
"""
