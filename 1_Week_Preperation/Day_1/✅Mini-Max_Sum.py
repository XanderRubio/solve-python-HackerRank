#!/bin/python3

import math
import os
import random
import re
import sys


def miniMaxSum(arr):
    max_sum = max(arr)
    min_sum = min(arr)
    Sum = sum(arr)
    print(Sum - max_sum, Sum - min_sum)


if __name__ == "__main__":
    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)

# Sample Input
"""
1 2 3 4 5
"""
# Sample Output
"""
10 14
"""
