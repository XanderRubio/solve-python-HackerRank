def findMedian(arr):
    sorted_arr = sorted(arr)
    median_index = len(sorted_arr) // 2
    median = sorted_arr[median_index]
    return median


if __name__ == "__main__":
    fptr = open(os.environ["OUTPUT_PATH"], "w")

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = findMedian(arr)

    fptr.write(str(result) + "\n")

    fptr.close()

# Sample Input
"""
7
0 1 2 4 6 5 3
"""
# Sample Output
"""
3
"""
